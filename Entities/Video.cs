using WebPortal.Models;
using System.ComponentModel.DataAnnotations;

namespace WebPortal.Entities
{
    public class Video
    {
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(80)]
        public string Title { get; set; }
        public Genres Genre { get; set; }
    }
}