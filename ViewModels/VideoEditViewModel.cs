﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebPortal.Models;

namespace WebPortal.ViewModels
{
    public class VideoEditViewModel
    {
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(80)]
        public string Title { get; set; }

        [Display(Name = "File Genre")]
        public Genres Genre { get; set; }
    }
}
