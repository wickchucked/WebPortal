using WebPortal.Entities;
using System.Collections.Generic;

namespace WebPortal.Services
{
    public interface IVideoData
    {
        IEnumerable<Video> GetAll();
        Video Get(int id);
        void Add(Video newVideo);
    }
}