namespace WebPortal.Services
{
    public interface IMessageService
    {
         string GetMessage();
    }
}