﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models
{
    public enum Genres
    {
        None,
        Animated,
        Horror,
        Comedy,
        Romance,
        Action
    }
}
