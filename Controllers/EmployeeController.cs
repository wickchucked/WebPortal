using Microsoft.AspNetCore.Mvc;

namespace WebPortal.Controllers
{
    [Route("company/[controller]/[action]")]
    public class EmployeeController : Controller
    {
        public string Index()
        {
            return "Hello from Employee";
        }

        public ContentResult Name()
        {
            return Content("Ed");
        }

        public string Country()
        {
            return "United States";
        }
    }
}